import sys
import logging
import json
import maltoolbox
from maltoolbox.language import specification
from maltoolbox.language import languagegraph

logging.basicConfig(level=logging.DEBUG,
            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%m-%d %H:%M',
            filename='tmp/log.txt',
            filemode='w')

logging.getLogger('python_jsonschema_objects').setLevel(logging.WARNING)
logger = logging.getLogger(__name__)
lang_spec = specification. \
    load_language_specification_from_mar('./org.mal-lang.coreLang-1.0.0.mar')

lang_graph = languagegraph.LanguageGraph()
lang_graph.generate_graph(lang_spec)
lang_graph.save_to_file('tmp/lang_graph.json')
