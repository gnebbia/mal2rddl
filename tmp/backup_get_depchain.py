def get_depchain_for_attack(lang_graph, asset, atkname):
    depchain = []
    atk = None
    for atk in lang_graph.attack_steps:
        if atk.name == asset + ":" + atkname:
            break

    atk.name

    # Get parents names
    # I have to loop through the parent nodes
    # that are keys of atk.parents



    for name, dep_tuples in atk.parents.items():
        for dep_tuple in dep_tuples:
            dep = dep_tuple[1]
            while dep:
                match (dep[0]):
                    case 'union' | 'intersection' | 'difference':
                        pass

                    case 'field':
                        fieldname = dep[1]
                        relname = dep[2].name
                        assleft = dep[2].fields[0][0]
                        assright = dep[2].fields[1][0]
                        print("RELNAME")
                        print(relname)
                        print("FIELDNAME")
                        print(fieldname)

                        # Get all subtypes
                        lass_list = get_subclassing_assets(assleft)
                        rass_list = get_subclassing_assets(assright)
                        lass_list_names = []
                        rass_list_names = []

                       # 
                       #for side in range(0, 2):
                       #    if association.fields[side][1] == associations_chain[1]:
                       #        opposite_fieldname = association.fields[1 - side][1]


                        print("LEFT ASSETS")
                        for asset in lass_list:
                            print(asset.name)
                            lass_list_names.append(asset.name)

                        print("RIGHT ASSETS")
                        for asset in rass_list:
                            print(asset.name)
                            rass_list_names.append(asset.name)


                        newdict = {"fieldname": fieldname, "relname": relname,
                                "assleft": lass_list_names, "assright": rass_list_names}

                        depchain.append(newdict)
                        print()
                        print()
                        print()
                        dep = dep[-1]

                    case 'transitive':
                        pass

                    case 'subType':
                        pass

                    case _:
                        print(f'Found unknown tuple type {dep[0]} that we do '
                            'not know how to handle!')
    return depchain
