# -*- encoding: utf-8 -*-
# dynp v0.1.0
# Generate DynaMAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione, Viktor Engström.
# See /LICENSE for licensing information.

"""
dynp language Module

:Copyright: 2021, Giuseppe Nebbione, Viktor Engström.
:License: GPLv3 (see /LICENSE).
"""

import json
from copy import deepcopy


def load_lang_spec(lang_spec: str) -> dict:
    """
    Read a DynaMAL language JSON specification file

    Arguments:
    lang_spec       - a language specification file that can be for example
                      provided by dync

    Return:
    A dictionary representing the whole grammar of the MAL language
    """
    with open(lang_spec, 'r', encoding='utf-8') as spec:
        data = spec.read()
    return json.loads(data)


def save_lang_spec(lang_spec: dict, filename: str) -> dict:
    """
    Saves a DynaMAL language specification dictionary to a JSON file

    Arguments:
    file_spec       - a language specification file that can be for example
                      provided by dync
    filename        - the JSON filename that will be saved
    """
    lang = {}
    lang['name'] = lang_spec['defines']['id']
    lang['categories'] = get_action_tree(lang_spec)
    lang['associations'] = []
    for assoc in lang_spec["associations"]:
        del assoc['meta']
        lang['associations'].append(assoc)


    with open(filename, 'w', encoding='utf-8') as file:
        json.dump(lang, file, indent=4)


def get_categories(lang_spec: dict) -> list:
    """
    Get all available object classes in the language specification

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      DynaMAL language as provided by `read_language_spec(...)`

    Return:
    A list of strings representing the available classes in the language
    """
    return list(lang_spec['categories'].keys())

def get_classes_by_category(lang_spec: dict, category: str) -> list:
    """
    Get all available object classes for a specified category

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      DynaMAL language as provided by `read_language_spec(...)`
    category        - the name of a category containing classes

    Return:
    A list of strings representing the available classes for the
    specified category
    """
    return list(lang_spec['categories'][category].keys())

def get_classes(lang_spec: dict) -> list:
    """
    Get all available object classes in the language specification

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      DynaMAL language as provided by `read_language_spec(...)`

    Return:
    A list of strings representing the available classes in the language
    specification
    """
    categories = get_categories(lang_spec)
    classes = []
    for cat in categories:
        classes += get_classes_by_category(lang_spec, cat)
    return classes



def get_super_classes(lang_spec: dict, object_class: str) -> list:
    """
    Get all the parent classes related to a class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a DynaMAL language as provided by `read_lang_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the parent classes

    Return:
    A list representing the parent classes of the provided class.
    The list is ordered from left to right, e.g.,
    ['MotherClass', 'GrandMotherClass', 'GrandGrandMotherClass']
    """
    super_classes = []

    def _get_super_classes(object_class):
        nonlocal super_classes
        for cat in lang_spec["categories"]:
            for asset_name,asset_data in lang_spec["categories"][cat].items():
                if asset_name == object_class and asset_data["extends"]:
                    super_classes.append(asset_data["extends"])
                    _get_super_classes(asset_data["extends"])

    _get_super_classes(object_class)
    return super_classes

def get_sub_classes(lang_spec: dict, object_class: str) -> list:
    """
    Get all the child classes related to a class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a DynaMAL language as provided by `read_lang_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the parent classes

    Return:
    A list representing the child classes of the provided class.
    ['Child1', 'Child2', 'Child3']
    """
    super_classes = []

    def _get_sub_classes(object_class):
        nonlocal super_classes
        for cat in lang_spec["categories"]:
            for asset_name,asset_data in lang_spec["categories"][cat].items():
                if asset_name == object_class and asset_data["extends"]:
                    super_classes.append(asset_data["extends"])
                    _get_sub_classes(asset_data["extends"])

    _get_super_classes(object_class)
    return super_classes


def get_action_type(lang: dict, objclass: str, act_name: str) -> dict:
    """
    Returns the action type associated to a specific action name
    """
    actions = get_actions_for_class(lang, objclass)
    return actions[act_name]['type']


def get_actions_for_class(lang_spec: dict, object_class: str) -> dict:
    """
    Get all Actions for a specific Class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      DynaMAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to list
                      the possible action steps

    Return:
    A dictionary representing the set of possible actions for the specified
    class. Each key in the dictionary is an action name and is associated
    to a dictionary containing other characteristics of the action such as
    type of action, TTC distribution, child action steps and other information
    """
    actions = {}

    category = lang_spec["category-table"][object_class]
    asset_data = lang_spec['categories'][category][object_class]
    action_space = asset_data['action-space']
    return action_space

def get_consequence_steps(lang: dict, objclass: str, act_name: str) -> dict:
    """
    Provides the dictionary of consequence steps for a specific action
    step in a language
    """
    actions = get_actions_for_class(lang, objclass)
    consequences = None
    try:
        consequences = actions[act_name]['actions']
    except TypeError:
        pass
    return consequences

def show_child_steps(
        lang: dict,
        object_class: str,
        act_name: str) -> list:
    """
    Provides the list of child action steps belonging to an action

    Arguments:
    lang            - the MAL (malc) language specification of an action step.
    object_class    - a string representing the class for which we want to
                      retrieve the possible action steps
    act_name        - a string representing the action for which we want to
                      retrieve the child action steps (e.g., "localConnect")

    Return:
    A list of strings, each representing a child action steps of the provided
    action name
    """
    actsteps = []

    def _get_child_steps(objclass):
        nonlocal actsteps
        actions = get_actions_for_class(lang, objclass)
        try:
            consequences = actions[act_name]['actions']
            for conseq in consequences:
                (optype, action_data), = conseq.items()
                print("ACTION DATA:")
                print(action_data)
                print("OPTYPE:")
                print(optype)
                for action in action_data:
                    action_name = optype + " "
                    print("ACTION:")
                    print(action)
                    for bit in action:
                        (_,strbit), = bit.items()
                        action_name += strbit
                    actsteps.append(action_name)
        except TypeError:
            pass

    _get_child_steps(object_class) 
    return actsteps


def get_associations_for_class(lang_spec: dict, object_class: str) -> list:
    """
    Get all the possible associations related to a class

    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      a MAL language as provided by `read_language_spec(...)`
    object_class    - a string representing the class for which we want to
                      retrieve the possible associations

    Return:
    A list of dictionaries representing the possible associations that are
    available for a specific class
    """
    assocs = []

    def _get_assocs(object_class):
        nonlocal assocs
        for assoc,attribs in lang_spec["associations"].items():
            if object_class in [attribs["leftAsset"], attribs["rightAsset"]]:
                assocs.append({
                    "name": assoc,
                    "leftClass":  attribs["leftAsset"],
                    "leftField":  attribs["leftRole"],
                    "rightClass": attribs["rightAsset"],
                    "rightField": attribs["rightRole"]})

    classes = get_super_classes(lang_spec, object_class)
    classes.insert(0, object_class)

    for objclass in classes:
        _get_assocs(objclass)

    # Remove Duplicate Associations
    assocs = [dict(t) for t in {tuple(d.items()) for d in assocs}]
    return assocs




def get_dependencies_for_action(
        lang: dict,
        object_class: str,
        act_name: str) -> list:
    """
    Provides the list of child action steps belonging to an action

    Arguments:
    lang            - the language specification
    object_class    - a string representing the class for which we want to
                      retrieve the possible action steps
    act_name        - a string representing the action for which we want to
                      retrieve the child action steps (e.g., "localConnect")

    Return:
    A list of strings, each representing a child action steps of the provided
    action name
    """
    return lang['dependency-table'][object_class][act_name]


def get_asset_class_from_rolename(
        lang: dict,
        origin_class: str,
        rolename: str) -> list:
    """
    Provides the destination asset class type starting from
    an origin class and a rolename.
    This answers to the question, what class is associated
    to class "Computer" with rolename "connection"?

    Arguments:
    lang            - the language specification
    origin_class    - a string representing the origin class
    rolename        - a string representing the rolename

    Return:
    A string representing the destination asset name corresponding
    to the role
    """
    return lang['role-table'][origin_class][rolename]

def get_rolename_from_assets(
        lang: dict,
        origin_class: str,
        dest_class: str,
        rolename: str) -> str:
    """
    Provides the destination asset class type starting from
    an origin class and a rolename.
    This answers to the question, what class is associated
    to class "Computer" with rolename "connection"?

    Arguments:
    lang            - the language specification
    origin_class    - a string representing the origin class
    dest_class      - a string representing the destination class
    rolename        - a string representing the left rolename

    Return:
    A string representing the destination asset name corresponding
    """
    parent_classes = get_super_classes(lang, origin_class)
    print("ORIGIN CLASS IS :")
    print(origin_class)
    print("DEST CLASS IS :")
    print(dest_class)
    print("ROLENAME")
    print(rolename)

    print("PARENTS_________-----ORIGIN ---------________________-")
    print(parent_classes)
    print("PARENTS_________-----DEST ---------________________-")
    parent_classes2 = get_super_classes(lang, dest_class)
    print(parent_classes2)
    for role, cls in lang['role-table'][dest_class].items():
        if cls == origin_class or cls in parent_classes:
            return role
        

def get_association_from_rolename(
        lang: dict,
        origin_class: str,
        rolename: str,
        dest_class: str) -> str:
    """
    Provides the destination asset class type starting from
    an origin class and a rolename.
    This answers to the question, what class is associated
    to class "Computer" with rolename "connection"?

    Arguments:
    lang            - the language specification
    origin_class    - a string representing the origin class
    rolename        - a string representing the rolename

    Return:
    A string representing the relationship name corresponding
    to the role
    """
    for relname, role in lang['association-table'][origin_class].items():
        # if len(role) == 1:
        #     (role_str, dest_class_str), = role.items()
        #     if rolename == role_str and dest_class_str == dest_class:
        #         return relname
        # else:
        for role_str, dest_class_str in role.items():
            if rolename == role_str and dest_class_str == dest_class:
                return relname


def get_attack_tree(lang_spec: dict):
    """
    Gives the attack tree of a whole language specification
    in the form of a dictionary of dictionaries.
    The keys of this dictionary represent the available
    classes in the language. To each key (class) 
    is associated a dictionary whose keys are attacks
    and whose values are lists of child attack steps.


    Arguments:
    lang_spec       - a dictionary representing the whole grammar of the
                      mal language as provided by `read_language_spec(...)`


    Return:
    A dictionary representing the whole attack tree of the input language
    """
    attack_tree = {}

    for cls in get_classes(lang_spec):
        attack_tree[cls] = {}
        for atk,attribs in get_actions_for_class(lang_spec, cls).items():
            atk_steps = get_consequence_steps(lang_spec, cls, atk)
            print(atk_steps)
            attack_tree[cls][atk] = {}
            attack_tree[cls][atk]["child_steps"] = atk_steps 
            attack_tree[cls][atk]["type"] = attribs['type'] 
            if attribs['type'] in ("exist","notExist"):
                required_steps = get_required_steps(lang_spec, cls, atk)
                attack_tree[cls][atk]["required_for_existence"] = required_steps

    return attack_tree
