import language as l
import sys
import json
import maltoolbox as mt
from maltoolbox.language import specification as spec
from maltoolbox.language import languagegraph as lgraph



# def read_lang_graph(filename):
#     with open(filename, 'r', encoding='utf-8') as spec:
#         data = spec.read()
#     return json.loads(data)
# 
# lg = read_lang_graph("tmp/lang_graph.json")



lang_spec = spec. \
    load_language_specification_from_mar('./org.mal-lang.examplelang2-1.0.0.mar')

lang_graph = lgraph.LanguageGraph()
lang_graph.generate_graph(lang_spec)
lang_graph.save_to_file('tmp/example_lang_graph.json')







# try:
#     lang = l.load_lang_spec(sys.argv[1])
# except Exception:
#     lang = l.load_lang_spec("exampleLang2.json")


# 
# 
# 
# rddl = {}
# 
# # Write begin of RDDL boilerplate
# 
# begin_rddl = "domain simple_compromise {"
# 
# end_header = " }"
# 
# 
# # Write requirements
# requirements = "partially-observed"
# 
# req_str = """
#     requirements {
#         partially-observed
#     };
# """
# 
# 
# # Write types
# types = l.get_classes(lang)
# 
# types_begin = """
#     types {
# """
# 
# types_interm = []
# for t in types:
#     types_interm.append(f"\t{t}: object;")
# 
# types_end = """
#     };
# """
# 
# types_str = types_begin + '\n'.join(types_interm) + types_end
# 
# 
# pvar_begin_str = """
#     pvariables {
# """
# pvar_end_str = """
#     };
# """
# 
# assocs = []
# 
# for name,attribs in lang['associations'].items():
#     left=attribs['leftAsset']
#     right=attribs['rightAsset']
#     assocs.append(f"\t{name}__{left}_{right}({left},{right}): {{ non-fluent, bool, default=false}};")
# 
# 
# 
# pvariables = []
# 
# state_strings = []
# action_strings = []
# obs_strings = []
# 
# 
# # Write pvariables
# for cls in types:
#     for atkname, val in l.get_actions_for_class(lang, cls).items():
#         atktype = val['type']
#         state_strings.append(f"\tstate_{cls}_{atkname}({cls}): {{ state-fluent, bool, default = false }};")
#         action_strings.append(f"\taction_{cls}_{atkname}({cls}): {{ action-fluent, bool, default = false }};")
#         obs_strings.append(f"\tobs_{cls}_{atkname}({cls}): {{ observ-fluent, bool }};")
# 
# 
# 
# # TO WRITE
# # State_strings
# # Action_strings
# # Obs_strings
# 
# 
# # Write cpfs for step
atkname = 'access'
asset = 'Network'
# deps = l.get_dependencies_for_action(lang, asset, atkname)
# self_deps = [x for x in deps if x[0] == 'self']
# ext_deps = [x for x in deps if x[0] != 'self']
# atktype = '|'
# self_deps
# ext_deps
# 
atk = None
for atk in lang_graph.attack_steps:
    if atk.name == asset + ":" + atkname:
        break

atk.name

# Get parents names
# I have to loop through the parent nodes
# that are keys of atk.parents

for name, dep_tuples in atk.parents.items():
    for dep_tuple in dep_tuples:
        dep = dep_tuple[1]
        while dep:
            match (dep[0]):
                case 'union' | 'intersection' | 'difference':
                    pass

                case 'field':
                    fieldname = dep[1]
                    relname = dep[2].name
                    assleft = dep[2].fields[0][0]
                    assright = dep[2].fields[1][0].name
                    print("RELNAME")
                    print(relname)
                    print("FIELDNAME")
                    print(fieldname)
                    print("ASSLEFT")
                    #print(assleft.name)
                    # Get all subtypes
                    assets = [assleft]
                    res_assets = [assleft]
                    # Give me the son classes
                    while assets:
                        asset = assets.pop()
                        assets.extend(asset.sub_assets)
                        res_assets.extend(asset.sub_assets)
                    for asset in res_assets:
                        print(asset.name)
                    print("ASSRIGHT")
                    print(assright)
                    print()
                    print()
                    print()
                    dep = dep[-1]

                case 'transitive':
                    pass

                case 'subType':
                    pass

                case _:
                    print(f'Found unknown tuple type {dep[0]} that we do '
                        'not know how to handle!')

# First Parent
#atk.parents["User:phish"][0]
# First dependency chain

fieldname
relname

assleft
assright

# Second dependency chain


# 
# 
# cpfs_begin_str = """
#     cpfs {
# 
# """
# 
# 
# deps1 = [['Network', 'access'], ['self', 'auth'], ['Host', 'compromise']]
# deps2 = [['Network', 'access'], ['Firewall', 'config'], ['Host', 'login']]
# deps3 = [['self', 'access'], ['self', 'config'], ['self', 'login']]
# 
# deps = deps3
# asset="Host"
# atkname="access"
# deps = l.get_dependencies_for_action(lang, asset, atkname)
# 
# # Add conditional content to template_state_begin
# template_state_interm = []
# template_obs_interm = []
# 
# self_deps = [x for x in deps if x[0] == 'self']
# ext_deps = [x for x in deps if x[0] != 'self']
# 
# 
# 
# def get_or_conditions(cls, deps):
#     template_state_interm = []
#     template_obs_interm = []
#     self_deps = [x for x in deps if x[0] == 'self']
#     ext_deps = [x for x in deps if x[0] != 'self']
#     for dep in self_deps:
#         depname = dep[1]
#         template_state_interm.append(f" state_{cls}_{depname}(?h) ")
#         template_obs_interm.append(f" state_{cls}_{depname}'(?h) ")
# 
# 
#     if template_state_interm:
#         template_state_interm = "^ (" + '|'.join(template_state_interm) + "))"
# 
# 
#         template_obs_interm = "^ (" + '|'.join(template_obs_interm) + "))"
# 
# 
#         return template_state_interm, template_obs_interm
#     
# 
# def get_and_conditions(cls, deps):
#     template_state_interm = []
#     template_obs_interm = []
#     self_deps = [x for x in deps if x[0] == 'self']
#     ext_deps = [x for x in deps if x[0] != 'self']
#     for dep in self_deps:
#         cls = dep[0]
#         depname = dep[1]
#         template_state_interm.append(f" state_{cls}_{depname}(?h) ")
#         template_obs_interm.append(f" state_{cls}_{depname}'(?h) ")
# 
# 
#     if template_state_interm:
#         template_state_interm = "^ (" + '^'.join(template_state_interm) + "))"
# 
# 
#         template_obs_interm = "^ (" + '^'.join(template_obs_interm) + "))"
# 
# 
#         return template_state_interm, template_obs_interm
# 
# 
# 
# def write_cpf(cls, atkname, atktype, deps):
#     template_state_begin = f"""
# 
#     state_{cls}_{atkname}'(?h) =
#         if ( ~state_{cls}_{atkname}(?h) ^ action_{cls}_{atkname}(?h) """
# 
#     template_state_end = f"""
#             then true
#         else state_{cls}_{atkname}(?h);
#     """
# 
#     template_obs_begin = f"""
# 
#     obs_{cls}_{atkname}(?h) =
#         if ( ~state_{cls}_{atkname}'(?h) """
# 
#     template_obs_end = f"""
#             then true
#         else state_{cls}_{atkname}'(?h);
# 
#     """
#     self_deps = [x for x in deps if x[0] == 'self']
#     ext_deps = [x for x in deps if x[0] != 'self']
#     if not deps or not self_deps:
#         template_state_begin += ")" + template_state_end
#         template_obs_begin   += ")" + template_obs_end
#         return template_state_begin, template_obs_begin
# 
#     if atktype == '|':
#         state_conds,obs_conds = get_or_conditions(cls, deps)
#     elif atktype == '&':
#         state_conds,obs_conds = get_and_conditions(cls, deps)
# 
#     template_state_begin += state_conds
#     template_state_begin += template_state_end
#     template_obs_begin += obs_conds
#     template_obs_begin += template_obs_end
# 
#     return template_state_begin, template_obs_begin
# 
# 
# 
# 
# # Write pvariables
# cpfs = []
# for cls in types:
#     for atkname, val in l.get_actions_for_class(lang, cls).items():
#         atktype = val['type']
#         deps = l.get_dependencies_for_action(lang, cls, atkname)
#         cpfs_state, cpfs_obs = write_cpf(cls, atkname, atktype, deps)
#         cpfs.append(cpfs_state)
#         cpfs.append(cpfs_obs)
# 
# 
# 
# 
# 
# 
# 
# 
# l.get_classes(lang)
# 
# # Get a list of the attack steps for a class
# l.get_actions_for_class(lang, 'Host').keys()
# 
# # Get action type 
# l.get_action_type(lang, 'Network', 'access')
# 
# # Get dependencies
# l.get_dependencies_for_action(lang, 'Host', 'access')
# 
# 
# 
# # Print the whole RDDL Spec
# 
# print(begin_rddl)
# print(req_str)
# print(types_str)
# print(pvar_begin_str)
# print('\n'.join(assocs))
# print('\n'.join(state_strings))
# print()
# print()
# print('\n'.join(action_strings))
# print()
# print()
# print('\n'.join(obs_strings))
# print(pvar_end_str)
# print(cpfs_begin_str)
# print('\n'.join(cpfs))
# print(pvar_end_str)
# print(end_header)
# 
# 
# 
# ## 
# 
# # Application:[(REL3, AS3, v, AS4, z), (REL2, AS2, t, AS3, y), (REL1, AS1, s, AS2, x)]:sqli
# # Application:[(none, AS4, self, none, none)]:unsanitizedInput
