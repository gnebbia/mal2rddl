import string
import sys
import json
import itertools
from itertools import permutations
from maltoolbox.language import specification as spec
from maltoolbox.language import languagegraph as lgraph

mal_mar = "./malspecs/org.mal-lang.examplelang2-1.0.0.mar"
mal_mar = "./malspecs/org.mal-lang.coreLang-1.0.0.mar"

lang_spec = spec.load_language_specification_from_mar(mal_mar)

lang_graph = lgraph.LanguageGraph()
lang_graph.generate_graph(lang_spec)
lang_graph.save_to_file('tmp/example_lang_graph.json')

rddl_outfile = "tmp/outdomain.rddl"

rddl = {}

# Write begin of RDDL boilerplate

begin_rddl = "domain simple_compromise {"
end_header = " }"


# Write requirements
requirements = "partially-observed"

req_str = """
    requirements {
        partially-observed
    };
"""


# Write types
types = [asset.name for asset in lang_graph.assets]

types_begin = """
    types {
"""

types_interm = []
for t in types:
    types_interm.append(f"\t{t}: object;")

types_end = """
    };
"""

types_str = types_begin + '\n'.join(types_interm) + types_end


pvar_begin_str = """
    pvariables {
"""
pvar_end_str = """
    };
"""

assocs = []
rels = []

for assoc in lang_graph.associations:
    name = assoc.name
    lass=assoc.left_field.asset
    rass=assoc.right_field.asset
    lscls = [asset.name for asset in lass.get_all_subassets()]
    rscls = [asset.name for asset in rass.get_all_subassets()]
    for left in lscls:
        for right in rscls:
            assocs.append(f"\t{name}_{left}_{right}({left},{right}): {{ non-fluent, bool, default=false}};")
            relid = name + "_" + left + "_" + right
            rels.append(relid)


pvariables = []

state_strings = []
action_strings = []
obs_strings = []


# Write pvariables
for attack_step in lang_graph.attack_steps:
    attack_step_name = attack_step.name.replace(':', '_')
    cls = attack_step.asset.name
    state_strings.append(f"\tstate_{attack_step_name}({cls}): {{ state-fluent, bool, default = false }};")
    action_strings.append(f"\taction_{attack_step_name}({cls}): {{ action-fluent, bool, default = false }};")
    obs_strings.append(f"\tobs_{attack_step_name}({cls}): {{ observ-fluent, bool }};")


cpfs_begin_str = """
    cpfs {

"""

def translate_dep_chain_link(dep_link, current_asset, current_var, latest_var_nr):
    if not dep_link:
        return [(current_var, current_asset, "", "")]
    match (dep_link.type):
        case 'union':
            return []

        case 'intersection' | 'difference':
            return []

        case 'field':
            print(dep_link.to_dict())
            if dep_link.association.left_field.fieldname == \
                dep_link.fieldname:
                reverse_variables = True
                target_assets = dep_link.association.left_field.asset.get_all_subassets()
            else:
                reverse_variables = False
                target_assets = dep_link.association.right_field.asset.get_all_subassets()
            results = []
            for asset in target_assets:
                target_var_nr = latest_var_nr + 1
                latest_var_nr = target_var_nr
                target_var = asset.name + '_' + str(target_var_nr)
                var_str = f"?{target_var}: {asset.name}, "
                if reverse_variables:
                    var_pair = f"?{target_var},?{current_var}"
                    rels_str = f"{dep_link.association.name}_{asset.name}_{current_asset.name}({var_pair}) ^"
                else:
                    var_pair = f"?{current_var},?{target_var}"
                    rels_str = f"{dep_link.association.name}_{current_asset.name}_{asset.name}({var_pair}) ^"
                results_strs = translate_dep_chain_link(dep_link.next_link,
                    asset, target_var, latest_var_nr)
                for new_target_var, new_target_asset, new_var_str, \
                    new_rels_str in results_strs:
                    latest_var_nr = max(target_var_nr,
                        int(new_target_var.split('_')[-1]))
                    results.append((new_target_var,
                        new_target_asset,
                        var_str + new_var_str,
                        rels_str + new_rels_str))

            return results

        case 'transitive':
            return []

        case 'subType':
            return []

        case _:
            print(f'Found unknown tuple type {dep_link.type} that we do '
                'not know how to handle!')
            return []

def write_cpf2(attack_step):
    attack_step_name = attack_step.name.replace(':', '_')
    init_var_nr = 0
    init_var = attack_step.asset.name + '_' + str(init_var_nr)
    print("ANALYZING:")
    print(attack_step_name)
    cls, atkname = atkstep.name.split(":")
    template_state_begin = f"""

        state_{attack_step_name}'(?{init_var}) =
            if ( ~state_{attack_step_name}(?{init_var}) ^ action_{attack_step_name}(?{init_var}) """

    template_state_end = f"""
                then true
            else state_{attack_step_name}(?{init_var});
    """

    template_obs_begin = f"""

        obs_{attack_step_name}(?{init_var}) =
            if ( ~state_{attack_step_name}'(?{init_var}) """

    template_obs_end = f"""
                then true
            else state_{attack_step_name}'(?{init_var});

    """
    state_conds = ""
    obs_conds = ""

    if atkstep.type == 'or':
        operator = '|'
    elif atkstep.type == 'and':
        operator = '^'

    if attack_step.parents:
        state_conds += "^ ( "
        obs_conds += "^ ( "

    level1_indent ="\n\t\t\t"
    level2_indent ="\n\t\t\t\t"
    latest_var_nr = init_var_nr
    failed = False
    for parent in attack_step.parents:
        parent_attack_step_name = parent.replace(':', '_')
        state_conds += f"{level1_indent}( "
        obs_conds += f"{level1_indent}( "
        print(parent)
        for origin_attack_step, dep_chain in attack_step.parents[parent]:
            if dep_chain:
                results_strs = translate_dep_chain_link(
                    dep_chain,
                    attack_step.asset,
                    init_var,
                    latest_var_nr)
                if not results_strs:
                    failed = True
                    break
                for target_var, target_asset, var_str, rels_str in results_strs:
                    if (target_asset.is_subasset_of(origin_attack_step.asset)):
                        latest_var_nr = max(latest_var_nr, int(target_var.split('_')[-1]))
                        parent_attack_step_name = target_asset.name + '_' + \
                            parent.split(":")[-1]
                        state_conds += f"{level2_indent}exists_{{ {var_str[:-2]} }} [ {rels_str[:-2]} ^ " \
                            f"state_{parent_attack_step_name}(?{target_var})] | "
                        obs_conds += f"{level2_indent}exists_{{ {var_str[:-2]} }} [ {rels_str[:-2]} ^ " \
                            f"state_{parent_attack_step_name}'(?{target_var})] | "
            else:
                state_conds += f"{level2_indent}state_{parent_attack_step_name}(?{init_var}) |"
                obs_conds += f"{level2_indent}state_{parent_attack_step_name}'(?{init_var}) |"
        state_conds = state_conds[:-2] + f") {operator}\n"
        obs_conds = obs_conds[:-2] + f") {operator}\n"
        if failed:
            break

    if attack_step.parents:
        state_conds = state_conds[:-2] + ") "
        obs_conds = obs_conds[:-2] + ") "

    if failed:
        state_conds = ""
        obs_conds = ""

    template_state_begin += state_conds
    template_state_begin += ")" + template_state_end
    template_obs_begin += obs_conds
    template_obs_begin += ")" + template_obs_end


    return template_state_begin, template_obs_begin


# Write pvariables
cpfs = []

print("-========================")
for atkstep in lang_graph.attack_steps:
    cpfs_state, cpfs_obs = write_cpf2(atkstep)
    cpfs.append(cpfs_state)
    cpfs.append(cpfs_obs)

print("-========================")

reward_str = "    reward = (sum_{?h: HardwareVulnerability} [state_HardwareVulnerability_exploit(?h)]);"

# # Print the whole RDDL Spec
# 
with open(rddl_outfile, "w") as f:
    print(begin_rddl, file=f)
    print(req_str, file=f)
    print(types_str, file=f)
    print(pvar_begin_str, file=f)
    print('\n'.join(assocs), file=f)
    print('\n'.join(state_strings), file=f)
    print("\n", file=f)
    print("\n", file=f)
    print('\n'.join(action_strings), file=f)

    print("\n", file=f)
    print("\n", file=f)

    print('\n'.join(obs_strings), file=f)
    print(pvar_end_str, file=f)
    print(cpfs_begin_str, file=f)
    print('\n'.join(cpfs), file=f)
    print(pvar_end_str, file=f)
    print("\n", file=f)
    print(reward_str, file=f)
    print("\n", file=f)
    print(end_header, file=f)
